// app.js

// Import the HTTP module
const http = require('http');

// Define the hostname and port
const hostname = '0.0.0.0';
const port = 3000;

// Create an HTTP server
const server = http.createServer((req, res) => {
    // Set the response status code and content type
    res.statusCode = 200;
    res.setHeader('Content-Type', 'text/plain');
    
    // Send the "Hello World" message
    res.end('Hello World\n');
});

// Start the server and listen for incoming requests
server.listen(port, hostname, () => {
    console.log(`Server is running at http://${hostname}:${port}/`);
});
